﻿using System;
using System.Net;
using System.Net.Mail;

namespace WindowsService1.Commons
{
    public static class EmailHelper
    {
        public static void Send(string title, string body, bool IsBodyHtml = true, EmailFrom from = null)
        {
            try
            {
                from = from ?? new EmailFrom();
                var smtpClient = new SmtpClient(from.Host, from.Port);
                smtpClient.Credentials = new NetworkCredential(from.UserName, from.Password);
                MailMessage msg = new MailMessage(from.DefaultFromAddress, ConfigHelper.DefaultReceiveMail, title, body);
                msg.IsBodyHtml = IsBodyHtml;
                msg.CC.Add(ConfigHelper.CC);
                smtpClient.Send(msg);
            }
            catch (Exception ex)
            {
                log4net.ILog log = log4net.LogManager.GetLogger("testApp.Logging");//获取一个日志记录器
                log.Info("邮件发送失败{0}", ex);//写入一条新log
                throw ex;
            }
        }
    }
}