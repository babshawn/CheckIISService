﻿using System;
using System.DirectoryServices;
using System.Net;
using System.Net.Sockets;
using System.ServiceProcess;

namespace WindowsService1.Commons
{
    /// <summary>
    /// 获取当前计算机相关信息
    /// </summary>
    public static class SystemInfo
    {
        /// <summary>
        /// 应用程序池检测
        /// </summary>
        public static string CheckAppPool()
        {
            // string method_Recycle = "Recycle"; //Start开启  Recycle回收  Stop 停止
            string entPath = "IIS://LOCALHOST/W3SVC/AppPools";
            var str = string.Empty;
            try
            {
                DirectoryEntry rootEntry = new DirectoryEntry(entPath);
                foreach (DirectoryEntry AppPool in rootEntry.Children)
                {
                    if (AppPool.Properties["AppPoolState"].Value.ToString() != "2")//是否有停止的应用程序池
                    {
                        //重启应用程序池
                        AppPool.Invoke("Start", null);
                        AppPool.CommitChanges();

                        str += AppPool.Name + ";";
                    }
                    AppPool.Close();
                }
            }
            catch (Exception ex)
            {
                LogHelper.WriteLog(ex.ToString());
                return str;
            }
            return str;
        }

        /// <summary>
        /// 检测网站是否可以访问
        /// </summary>
        /// <returns></returns>
        public static bool CheckUrlVisit()
        {
            try
            {
                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(ConfigHelper.WebSite);
                HttpWebResponse resp = (HttpWebResponse)req.GetResponse();
                if (resp.StatusCode == HttpStatusCode.OK)
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                LogHelper.WriteLog(ex.ToString());
                return false;
            }
            return false;
        }

        /// <summary>
        /// 重启IIS
        /// </summary>
        public static void StartIIS()
        {
            try
            {
                ServiceController sc = new ServiceController("iisadmin");
                if (sc.Status != ServiceControllerStatus.Running)
                {
                    sc.Start();
                }
            }
            catch (Exception ex)
            {
                LogHelper.WriteLog(ex.ToString());
            }
        }

        /// <summary>
        /// 获取当前计算机信息   例如:计算机名:aaa,IP:10.10.10.10
        /// </summary>
        /// <returns></returns>
        public static string GetComputerInfo()
        {
            try
            {
                string HostName = Dns.GetHostName(); //得到主机名
                IPHostEntry IpEntry = Dns.GetHostEntry(HostName);
                for (int i = 0; i < IpEntry.AddressList.Length; i++)
                {
                    //从IP地址列表中筛选出IPv4类型的IP地址
                    //AddressFamily.InterNetwork表示此IP为IPv4,
                    //AddressFamily.InterNetworkV6表示此地址为IPv6类型
                    if (IpEntry.AddressList[i].AddressFamily == AddressFamily.InterNetwork)
                    {
                        return string.Format("服务器: {0},IP: {1}", HostName, IpEntry.AddressList[i].ToString());
                    }
                }
                return string.Empty;
            }
            catch (Exception ex)
            {
                LogHelper.WriteLog(ex.ToString());
                return string.Empty;
            }

        }
    }
}