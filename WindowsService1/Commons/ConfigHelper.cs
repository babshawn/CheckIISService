﻿using System.Configuration;

namespace WindowsService1.Commons
{
    public static class ConfigHelper
    {
        public static string EmailHost
        {
            get { return ConfigurationManager.AppSettings.Get("EmailHost") ?? ""; }
        }

        public static string EmailPort
        {
            get { return ConfigurationManager.AppSettings.Get("EmailPort") ?? "0"; }
        }

        public static string EmailUserName
        {
            get { return ConfigurationManager.AppSettings.Get("EmailUserName") ?? ""; }
        }

        public static string EmailPassword
        {
            get { return ConfigurationManager.AppSettings.Get("EmailPassword") ?? ""; }
        }

        public static string DefaultFromAddress
        {
            get { return ConfigurationManager.AppSettings.Get("DefaultFromAddress") ?? ""; }
        }

        public static string DefaultReceiveMail
        {
            get { return ConfigurationManager.AppSettings.Get("DefaultReceiveMail") ?? ""; }
        }

        public static string CC
        {
            get { return ConfigurationManager.AppSettings.Get("CC") ?? ""; }
        }

        public static string WebSite
        {
            get { return ConfigurationManager.AppSettings.Get("WebSite") ?? ""; }
        }

        public static double Interval
        {
            get { return double.Parse(ConfigurationManager.AppSettings.Get("Interval") ?? "900000"); }
        }
        public static bool IsEmail
        {
            get { return bool.Parse(ConfigurationManager.AppSettings.Get("IsEmail") ?? "true"); }
        }
        public static bool IsWebSite
        {
            get { return bool.Parse(ConfigurationManager.AppSettings.Get("IsWebSite") ?? "true"); }
        }
    }
}