﻿namespace WindowsService1.Commons
{
    public class EmailFrom
    {
        public EmailFrom()
        {
            Host = ConfigHelper.EmailHost;
            Port = int.Parse(ConfigHelper.EmailPort);
            UserName = ConfigHelper.EmailUserName;
            Password = ConfigHelper.EmailPassword;
            DefaultFromAddress = ConfigHelper.DefaultFromAddress;
            DefaultFromDisplayName = "FSM极服务";
        }

        public string Host { get; set; }

        public int Port { get; set; }

        public string UserName { get; set; }

        public string Password { get; set; }

        public string DefaultFromAddress { get; set; }

        public string DefaultFromDisplayName { get; set; }
    }
}