﻿namespace WindowsService1.Commons
{
    public class LogHelper
    {
        public static void WriteLog(string str)
        {
            log4net.ILog log = log4net.LogManager.GetLogger("testApp.Logging");//获取一个日志记录器
            log.Info(str);//写入一条新log
        }
    }
}