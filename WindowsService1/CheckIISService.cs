﻿using System;
using System.ServiceProcess;
using WindowsService1.Commons;

namespace WindowsService1
{
    public partial class CheckIISService : ServiceBase
    {
        //定时器
        private System.Timers.Timer t = null;

        public CheckIISService()
        {
            InitializeComponent();

            //启用暂停恢复
            base.CanPauseAndContinue = true;

            //每*秒执行一次
            t = new System.Timers.Timer(ConfigHelper.Interval);
            //设置是执行一次（false）还是一直执行(true)；
            t.AutoReset = true;
            //是否执行System.Timers.Timer.Elapsed事件；
            t.Enabled = true;
            //到达时间的时候执行事件(theout方法)；
            t.Elapsed += new System.Timers.ElapsedEventHandler(MonitoringISSAppPool);
        }

        protected override void OnStart(string[] args)
        {
            string state = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss") + "启动";
            LogHelper.WriteLog(state);
        }

        protected override void OnStop()
        {
            string state = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss") + "停止";
            LogHelper.WriteLog(state);
        }

        //恢复服务执行
        protected override void OnContinue()
        {
            string state = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss") + "继续";
            LogHelper.WriteLog(state);
            t.Start();
        }

        //暂停服务执行
        protected override void OnPause()
        {
            string state = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss") + "暂停";
            LogHelper.WriteLog(state);
            t.Stop();
        }

        public void Theout(object source, System.Timers.ElapsedEventArgs e)
        {
            LogHelper.WriteLog("theout:" + DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss"));
        }

        /// <summary>
        /// 检测当前线程池是否正常开启
        /// </summary>
        public void MonitoringISSAppPool(object source, System.Timers.ElapsedEventArgs e)
        {
            var computerInfo = SystemInfo.GetComputerInfo();

            #region 应用程序池检测

            if (ConfigHelper.IsEmail)
            {
                var appPoolName = SystemInfo.CheckAppPool();
                if (!string.IsNullOrEmpty(appPoolName))
                {
                    string body = string.Format("<html>监控到：{0} 应用程序池停止;<br/>已自动重启该应用程序池，{1}。</html>", appPoolName, computerInfo);
                    string title = "服务器重启应用程序池通知";
                    EmailHelper.Send(title, body, true);
                }
            }

            #endregion 应用程序池检测

            #region 站点检测

            if (ConfigHelper.IsWebSite)
            {
                var result = SystemInfo.CheckUrlVisit();
                if (!result)
                {
                    //重启IIS
                    SystemInfo.StartIIS();
                    string ibody = string.Format("<html>监控到站点：{0} 无法访问;<br/>已自动重启IIS服务，{1}。</html>", ConfigHelper.WebSite, computerInfo);
                    string ititle = "服务器站点无法访问通知";
                    EmailHelper.Send(ititle, ibody, true);
                }
            }

            #endregion 站点检测
        }
    }
}